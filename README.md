Contact Details: A WIP Joomla Plugin to manage the contact details for different groups.
===

Born from my unwillingness to manually update the contact details yearly for nearly 50 sports on the University of Strathclyde [Sports Union](http://www.sportsunion.co.uk) website.

Expands 3 tags inside [Joomla](http://www.joomla.org/) content blocks to inject HTML tables populated from a database.
Tags are hard coded at the moment. If I ever decided to publish this properly I'll make them configurable but it shouldn't be very hard to see how it works if you have a burning desire to use my plugin.

NB:This code is *not* production ready and will cause your site to fail to render if enabled.