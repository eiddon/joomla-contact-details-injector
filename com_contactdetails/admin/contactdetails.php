<?php
    function println($str = "") {
        echo $str."</br>";
    }

    function shortCode($str) {
        return preg_replace("/[\s_]/", "-", preg_replace("/[\s-]+/", " ", preg_replace("/[^a-z0-9_\s-]/", "", trim(strtolower($str)))));
    }

    function id($shortcode, $db) {
        $db->setQuery("SELECT * FROM #__contactdetails_sports where shortname='".$shortcode."'");
        $row = $db->loadAssoc();
        if($row !== null) {
            return $row['id'];
        }
        $query = $db->getQuery(true);
        $columns = array('shortname');
        $values = array($db->quote($shortcode));

        $query->insert($db->quoteName('#__contactdetails_sports'))->columns($db->quoteName($columns))->values(implode(',', $values));
        $db->setQuery($query);
        $db->query();
        $db->setQuery("SELECT * FROM #__contactdetails_sports where shortname='".$shortcode."'");
        $row = $db->loadAssoc();
        if($row !== null) {
            return $row['id'];
        }
        exit('Failed to find id for shortcode: '.$shortcode);
    }

    function e($str) {
        return str_replace("’", "'", $str);
    }

    function clean($db) {
        $db->setQuery("CREATE TABLE IF NOT EXISTS `#__contactdetails_sports` (
                        `id` int(11) NOT NULL AUTO_INCREMENT,
                        `shortname` varchar(100) NOT NULL,
                        `email` varchar(100) DEFAULT NULL,
                        `website` varchar(100) DEFAULT NULL,
                        `facebook` varchar(100) DEFAULT NULL,
                        `twitter` varchar(100) DEFAULT NULL,
                        `youtube` varchar(100) DEFAULT NULL,
                        PRIMARY KEY (`id`),
                        UNIQUE KEY `shortname` (`shortname`))");
        $db->execute();
        $db->setQuery("CREATE TABLE IF NOT EXISTS `#__contactdetails_people` (
                        `id` int(11) NOT NULL AUTO_INCREMENT,
                        `sport_id` int(11) NOT NULL,
                        `rank` int(11) DEFAULT NULL,
                        `rankname` varchar(100) DEFAULT NULL,
                        `name` varchar(100) NOT NULL,
                        `email` varchar(100) DEFAULT NULL,
                        `phone` varchar(100) DEFAULT NULL,
                        PRIMARY KEY (`id`),
                        KEY `sport_id` (`sport_id`),
                        CONSTRAINT `#__contactdetails_people_ibfk_1` FOREIGN KEY (`sport_id`) REFERENCES `#__contactdetails_sports` (`id`))");
        $db->execute();
        $db->setQuery("CREATE TABLE IF NOT EXISTS `#__contactdetails_training` (
                        `id` int(11) NOT NULL AUTO_INCREMENT,
                        `sport_id` int(11) NOT NULL,
                        `day` enum('Monday','Tuesday','Wednesday','Thursday','Friday') DEFAULT NULL,
                        `time` varchar(100) DEFAULT NULL,
                        `location` varchar(100) DEFAULT NULL,
                        `other_info` varchar(100) DEFAULT NULL,
                        PRIMARY KEY (`id`),
                        KEY `sport_id` (`sport_id`),
                        CONSTRAINT `#__contactdetails_training_ibfk_1` FOREIGN KEY (`sport_id`) REFERENCES `#__contactdetails_sports` (`id`))");
        $db->execute();
        $db->setQuery("DELETE FROM #__contactdetails_people");
        $db->execute();
        $db->setQuery("DELETE FROM #__contactdetails_training");
        $db->execute();
        $db->setQuery("DELETE FROM #__contactdetails_sports");
        $db->execute();
        $db->setQuery("ALTER TABLE #__contactdetails_sports AUTO_INCREMENT = 1");
        $db->execute();
    }

if(empty($_FILES)){
    if($_POST['wipe'] === 'wipe'){
        $db = JFactory::getDbo();
        clean($db);
    }
} else {
    if(isset($_FILES['tt'])){
        echo "<div style='overflow-y:scroll;height:400px'>";
        $handle = fopen($_FILES['tt']["tmp_name"], "r");
        $db = JFactory::getDbo();
        while(($arr = fgetcsv($handle)) !== false){
            if($arr[0] !== ''){
                $sport = shortCode($arr[0]);
                $sportid = id($sport, $db);
                $day = $db->quote($arr[1]);
                $time = $db->quote($arr[2]);
                $venue = $db->quote($arr[3]);
                $info = $db->quote($arr[4]);
                $str = $sport."(".$sportid.") ".$day." ".$time." ".$venue." ".$info;
                $db->setQuery("INSERT INTO #__contactdetails_training (sport_id, day, time, location, other_info) values (".$sportid.", ".$day.", ".$time.", ".$venue.", ".$info.")");
                $db->execute();
                println($str);
            }
        }
        echo "</div>";
    }
    else if(isset($_FILES['sacc'])){
        echo "<div style='overflow-y:scroll;height:400px'>";
        $handle = fopen($_FILES['sacc']["tmp_name"], "r");
        $db = JFactory::getDbo();
        if($_POST['wipe'] === 'wipe'){
            clean($db);
        }
        $sport = ''; //sportname
        $sportid; //sportid
        $rank; //rank counter
        while(($arr = fgetcsv($handle)) !== false){
            if($arr[0] !== ''){
                $sport = shortCode($arr[0]);
                $sportid = id($sport, $db);
                $rank = 0;
                continue;
            }
            if($sport === '')
                continue;
            if($arr[1] !== ''){
                $rank++;
                $name = $db->quote($arr[1]);
                $rankname = $db->quote($arr[2]);
                $email = $db->quote($arr[3]);
                $phone = $db->quote($arr[4]);
                $str = $sport."(".$sportid.") ".$arr[1]." ".$arr[2]." ".$arr[3]." ".$arr[4];
                $db->setQuery("INSERT INTO #__contactdetails_people (sport_id, rank, rankname, name, email, phone) values (".$sportid.", ".$rank.", ".$rankname.", ".$name.", ".$email.", ".$phone.")");
                $db->execute();
                println($str);
                continue;
            }
            if($arr[2] !== '') {
                $soc = shortCode($arr[2]);
                switch($soc) {
                    case "club-email":
                        $email = $db->quote($arr[3]);
                        $db->setQuery("UPDATE #__contactdetails_sports set email=".$email." where id=".$sportid);
                        $db->execute();
                        println($sport."(".$sportid.") ".$soc." ".$email);
                        break;
                    case "club-facebook":
                        $facebook = $db->quote($arr[3]);
                        $db->setQuery("UPDATE #__contactdetails_sports set facebook=".$facebook." where id=".$sportid);
                        $db->execute();
                        println($sport."(".$sportid.") ".$soc." ".$facebook);
                        break;
                    case "club-website":
                        $website = $db->quote($arr[3]);
                        $db->setQuery("UPDATE #__contactdetails_sports set website=".$website." where id=".$sportid);
                        $db->execute();
                        println($sport."(".$sportid.") ".$soc." ".$website);
                        break;
                    case "club-twitter":
                        $twitter = $db->quote($arr[3]);
                        $db->setQuery("UPDATE #__contactdetails_sports set twitter=".$twitter." where id=".$sportid);
                        $db->execute();
                        println($sport."(".$sportid.") ".$soc." ".$twitter);
                        break;
                    case "club-youtube":
                        $youtube = $db->quote($arr[3]);
                        $db->setQuery("UPDATE #__contactdetails_sports set youtube=".$youtube." where id=".$sportid);
                        $db->execute();
                        println($sport."(".$sportid.") ".$soc." ".$youtube);
                        break;
                }
            }
        }
        echo "</div>";
    }
    }
?>
<style>
    form{
        border: 1px solid black;
    }
</style>
<form action="" method="post" enctype="multipart/form-data">
    <label>Sports and Committee Contacts Wipe</label>
    <input type="file" name="sacc"/>
    <input type="submit"/>
    <input type="hidden" value="wipe" name="wipe"/>
</form>
<form action="" method="post" enctype="multipart/form-data">
    <label>Sports and Committee Contacts Add (Dumb)</label>
    <input type="file" name="sacc"/>
    <input type="submit"/>
    <input type="hidden" value="add" name="wipe"/>
</form>
<form action="" method="post" enctype="multipart/form-data">
    <label>Training Times</label>
    <input type="file" name="tt"/>
    <input type="submit"/>
</form>
<form action="" method="post" enctype="multipart/form-data">
    <label>Wipe!</label>
    <input type="hidden" value="wipe" name="wipe"/>
    <input type="submit"/>
</form>
