<?php
// define('_JEXEC') or die('Restricted access'); //Ensure we aren't being accessed directly.

class PlgContentContactdetails extends JPlugin
{
    public function onContentPrepare($context, &$row, &$params, $page = 0)
    {
        if (is_object($row))
        {
            return $this->_expand($row->text, $params);
        }

        return $this->_expand($row, $params);
    }

    /*
     *	Expand tags of the form [sport:contacts] into an html table {Position, Name, Email, Phone No.}.
     *	Expand tags of the form [sport:training] into an html table {Day, Time, Location, Other Information}.
     *	Expand tags of the form [sport:soc] into an html table {Email, Website, Facebook, Twitter}.
     */
    protected function _expand(&$text, &$params)
    {
        //$text = "sup?" . $text;
        $pattern = "/\[([a-z-]+):(contacts|training|soc)\]/";

        while(preg_match($pattern, $text, $regs, PREG_OFFSET_CAPTURE))
        {
            $sport = $regs[1][0];
            $field = $regs[2][0];
            $db = JFactory::getDbo();

            $db->setQuery("SELECT * FROM #__contactdetails_sports where shortname='".$sport."'");
            $row = $db->loadAssoc();

            $replacement = "";
            if($row !== null)
            {
                switch ($field) {
                    case "soc":
                        $replacement = $this->_socwrap($row);
                        break;
                    case "contacts":
                        $replacement = $this->_contactswrap($row, $db);
                        break;
                    case "training":
                        $replacement = $this->_trainingwrap($row, $db);
                        break;
                }
            }

            $text = substr_replace($text, $replacement, $regs[0][1], strlen($regs[0][0]));
        }

        return true;
    }

    protected function _socwrap($row)
    {
        return '<div class="CommitteeDetailsTable">'.$this->_soc($row).'</div>';
    }


    protected function _contactswrap($row, $db)
    {
        return '<div class="CommitteeDetailsTable">'.$this->_contacts($row, $db).'</div>';
    }


    protected function _trainingwrap($row, $db)
    {
        return '<div class="CommitteeDetailsTable">'.$this->_training($row, $db).'</div>';
    }

    protected function _soc($row)
    {
        $str = "";
        if($row['email'] !== null){
            $str = $str."<tr><td>email</td><td>".$row['email']."</td></tr>";
        }
        if($row['website'] !== null){
            $str = $str."<tr><td>website</td><td>".$row['website']."</td></tr>";
        }
        if($row['facebook'] !== null){
            $str = $str."<tr><td>facebook</td><td>".$row['facebook']."</td></tr>";
        }
        if($row['twitter'] !== null){
            $str = $str."<tr><td>twitter</td><td>".$row['twitter']."</td></tr>";
        }
        if($row['youtube'] !== null){
            $str = $str."<tr><td>youtube</td><td>".$row['youtube']."</td></tr>";
        }
        if($str !== "")
            $str = "<table>".$str."</table>";
        return $str;
    }

    protected function _contacts($row, $db)
    {
        $db->setQuery("SELECT * FROM #__contactdetails_people where sport_id=".$row['id']." ORDER BY rank,name ASC");
        $replacement = "";
        $rows = $db->loadAssocList();
        foreach($rows as $crow){
            $replacement = $replacement."<tr><td>".$crow['rankname']."</td><td>".str_replace('?', "'", $crow['name'])."</td><td>".$crow['email']."</td><td>".$crow['phone']."</td></tr>";
        }
        if($replacement !== ""){
            $replacement = "<table><thead><tr><th>Position</th><th>Name</th><th>e-mail</th><th>Phone</th></tr></thead><tbody>".$replacement."</tbody></table>";
        }
        return $replacement;
    }

    protected function _training($row, $db)
    {
        $db->setQuery("SELECT * FROM #__contactdetails_training where sport_id=".$row['id']." ORDER BY day,time ASC");
        $replacement = "";
        $rows = $db->loadAssocList();
        foreach($rows as $trow){
            $replacement = $replacement."<tr><td>".$trow['day'].
                "</td><td>".$trow['time'].
                "</td><td>".$trow['location'].
                "</td><td>".$trow['other_info'].
                "</td></tr>";
        }
        if($replacement !== ""){
            $replacement = "<table><thead><tr><th>Day</th><th>Time</th><th>Location</th><th>Other Information</th></tr></thead><tbody>".$replacement."</tbody></table>";
        }
        return $replacement;
    }
}

?>
